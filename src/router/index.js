import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import layout from '@/views/layout'
import login from '@/views/login'
import NotFound from '@/views/404'
import Home from '@/views/layout/home'
import QA from '@/views/layout/QA'

import User from '@/views/layout/user'
import Chat from '@/views/layout/user/chat'
import Updata from '@/views/layout/user/updata'

import Search from '@/views/search'
import SearchResult from '@/views/search/result'

import Video from '@/views/layout/video'
import Deltails from '@/views/deltails'
Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: login
  },
  {
    path: '/user/updata',
    component: Updata
  },
  {
    path: '/',
    component: layout,
    children: [
      {
        path: '',
        component: Home,
        meta: {
          isKeepAlive: true
        }
      },
      {
        path: 'qa',
        component: QA
      },
      {
        path: 'user',
        component: User
      },
      {
        path: 'video',
        component: Video
      }
    ]
  },
  {
    path: '/search',
    component: Search
  },
  {
    path: '/user/chat',
    component: Chat
  },
  {
    path: '/search/result',
    component: SearchResult
  },
  {
    path: '/deltails/:id',
    component: Deltails
  },
  {
    path: '*',
    component: NotFound
  }
]

const router = new VueRouter({
  routes
})

const loginUrls = ['/user', '/user/updata', '/user/chat']
router.beforeEach((to, from, next) => {
  const token = store.state.user.token.token
  if (token) {
    next()
    return
  }
  if (loginUrls.includes(to.path)) {
    router.push({
      path: '/login',
      query: {
        back: to.path
      }
    })
  } else {
    next()
  }
})

export default router
