import { getNavList, getAllChannelList } from '@/api/channel'
const state = {
  channelList: [],
  allChannelList: []
}
const mutations = {
  setChannelList(state, payload) {
    state.channelList = payload
  },
  setAllChannelList(state, payload) {
    state.allChannelList = payload
  },
  addChannel(state, payload) {
    state.channelList.push(payload)
  },
  delChannel(state, channel) {
    state.channelList = state.channelList.filter(item => item.id !== channel.id)
  }
}
const getters = {
  optionalChannelList(state) {
    return state.allChannelList.filter(
      item => state.channelList.findIndex(v => +v.id === +item.id) === -1
    )
  }
}
const actions = {
  async getNavList(context) {
    const res = await getNavList()
    // console.log(res.data.channels)
    context.commit('setChannelList', res.data.channels)
  },
  async getAllChannelList(context) {
    const res = await getAllChannelList()
    context.commit('setAllChannelList', res.data.channels)
  }
}
export default {
  state,
  mutations,
  getters,
  actions,
  namespaced: true
}
