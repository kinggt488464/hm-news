import { setToken, getToken, removeToken } from '@/utils/storage'
import { getUserInfo } from '@/api/user'
export default {
  namespaced: true,
  state: {
    token: getToken(),
    userInfo: {}
  },
  mutations: {
    setToken(state, token) {
      state.token = token
      // 保存token到localStorage中
      setToken(token)
    },
    removeToken(state) {
      state.token = {}
      removeToken()
    },
    setUserInfo(state, userInfo) {
      state.userInfo = userInfo
    }
  },
  actions: {
    async getUserInfo(content) {
      const res = await getUserInfo()
      // console.log(res)
      content.commit('setUserInfo', res.data)
    }
  }
}
