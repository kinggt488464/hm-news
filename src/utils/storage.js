const tokenName = 'hm-news-token-83'
const historyName = 'hm-news-history-83'

export function setToken(token) {
  localStorage.setItem(tokenName, JSON.stringify(token))
}

export function getToken() {
  return JSON.parse(localStorage.getItem(tokenName)) || {}
}

export function removeToken() {
  localStorage.removeItem(tokenName)
}
const channelName = 'hm-news-channel-83'

export function setChannelList(channelList) {
  localStorage.setItem(channelName, JSON.stringify(channelList))
}

export function getChannelList() {
  return JSON.parse(localStorage.getItem(channelName))
}
export function getHistory() {
  return JSON.parse(localStorage.getItem(historyName)) || []
}

export function setHistory(list) {
  localStorage.setItem(historyName, JSON.stringify(list))
}
