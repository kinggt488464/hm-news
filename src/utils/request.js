import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { Toast } from 'vant'
import JSONBig from 'json-bigint'
// import router from '@/router'
const baseURL = 'http://toutiao-app.itheima.net'
// const baseURL = 'http://localhost:8000'
const request = axios.create({
  baseURL,
  timeout: 5000,
  transformResponse: [
    function(data) {
      try {
        return JSONBig.parse(data)
      } catch {
        return data
      }
    }
  ]
})

// 添加请求拦截器
request.interceptors.request.use(
  function(config) {
    // 在发送请求之前做些什么
    const token = store.state.user.token.token
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    // console.log(config)
    return config
  },
  function(error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)
// 添加响应拦截器
request.interceptors.response.use(
  function(response) {
    // 对响应数据做点什么
    return response.data
  },
  async function(error) {
    // 对响应错误做点什么
    function goLogin() {
      store.commit('user/removeToken')
      router.push({
        path: '/login',
        query: {
          back: router.currentRoute.path
        }
      })
      Toast.fail('登录信息失效')
    }
    // console.dir(error)
    if (error.response.status === 401) {
      const tokenObj = store.state.user.token
      if (!tokenObj.refresh_token) {
        // 跳转到登录页
        goLogin()
      }
      try {
        // 有刷新token
        // 发送请求刷新token,,,,需要用原始的axios发送请求
        const res = await axios({
          method: 'put',
          url: baseURL + '/v1_0/authorizations',
          headers: {
            Authorization: 'Bearer ' + tokenObj.refresh_token
          }
        })
        // console.log(res)
        // 保存新的token
        store.commit('user/setToken', {
          token: res.data.data.token,
          refresh_token: tokenObj.refresh_token
        })
        // 重新发送请求，并且返回请求的结果
        return request({
          method: error.config.method,
          url: error.config.url
        })
      } catch {
        // 如果刷新token失败了，跳转到登录页
        goLogin()
      }
    }
  }
)

export default request
