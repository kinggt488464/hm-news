import axios from '@/utils/request'
/**
 * 登录接口
 * @param {*} mobile 手机号
 * @param {*} code 验证码
 * @returns
 */
export function login(mobile, code) {
  return axios({
    method: 'post',
    url: '/v1_0/authorizations',
    data: {
      mobile,
      code
    }
  })
}
/**
 *用户信息接口
 * @returns
 */
export function getUserInfo() {
  return axios({
    method: 'get',
    url: '/v1_0/user/profile'
  })
}
export function updataInfo(data) {
  return axios({
    method: 'PATCH',
    url: '/v1_0/user/profile',
    data: data
  })
}
export function uploadPhoto(photo) {
  return axios({
    method: 'PATCH',
    url: '/v1_0/user/photo',
    data: photo
  })
}
