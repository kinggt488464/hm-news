import axios from '@/utils/request'
/**
 * 获取频道列表接口
 * @returns
 */
export function getNavList() {
  return axios({
    method: 'get',
    url: '/v1_0/user/channels'
  })
}
/**
 * 获取全部频道
 * @returns
 */
export const getAllChannelList = () => {
  return axios({
    method: 'get',
    url: '/v1_0/channels'
  })
}
/**
 * 添加频道
 * @param {*} channels
 * @returns
 */
export function addChannel(channels) {
  return axios({
    method: 'PATCH',
    url: '/v1_0/user/channels',
    data: {
      channels
    }
  })
}

/**
 * 删除频道
 * @param {'*'} id
 * @returns
 */
export function delChannel(id) {
  return axios({
    method: 'DELETE',
    url: `/v1_0/user/channels/${id}`
  })
}
