import axios from '@/utils/request'
/**
 *频道新闻推荐接口
 * @param {*} id
 * @param {*} timestamp
 * @returns
 */
export function getArticleList(id, timestamp) {
  return axios({
    method: 'get',
    url: '/v1_1/articles',
    params: {
      channel_id: id,
      timestamp,
      with_top: 1
    }
  })
}
/**
 * 对文章不感兴趣接口
 * @param {*} id
 * @returns
 */
export function dislikeArticle(id) {
  return axios({
    method: 'post',
    url: '/v1_0/article/dislikes',
    data: {
      target: id
    }
  })
}
/**
 * 文章举报接口
 * @param {*} id
 * @param {*} type
 * @returns
 */
export function reportArticle(id, type) {
  return axios({
    method: 'post',
    url: '/v1_0/article/reports',
    data: {
      target: id,
      type
    }
  })
}
/**
 * 获取文章详情
 * @param {*} id
 * @returns
 */
export function getArticleDeltails(id) {
  return axios({
    method: 'get',
    url: '/v1_0/articles/' + id
  })
}
/**
 * 关注用户
 * @param {*} id
 * @returns
 */
export function followUser(id) {
  return axios({
    method: 'POST',
    url: '/v1_0/user/followings',
    data: {
      target: id
    }
  })
}
/**
 * 取消关注
 * @param {*} id
 * @returns
 */
export function unFollowUser(id) {
  return axios({
    method: 'DELETE',
    url: '/v1_0/user/followings/' + id
  })
}
/**
 * 取消点赞
 * @param {*} id 文章编号
 */
export function deleteLike(id) {
  return axios({
    method: 'delete',
    url: '/v1_0/article/likings/' + id
  })
}

/**
 * 添加点赞
 * @param {*} id 文章编号
 */
export function addLike(id) {
  return axios({
    method: 'post',
    url: '/v1_0/article/likings',
    data: {
      target: id
    }
  })
}

/**
 * 取消不喜欢
 * @param {*} id 文章编号
 */
export function deleteDisLike(id) {
  return axios({
    method: 'delete',
    url: '/v1_0/article/dislikes/' + id
  })
}

/**
 * 添加不喜欢
 * @param {*} id 文章编号
 */
export function addDisLike(id) {
  return axios({
    method: 'post',
    url: '/v1_0/article/dislikes',
    data: {
      target: id
    }
  })
}
