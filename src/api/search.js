import axios from '@/utils/request'
export function getSuggestion(keyword) {
  return axios({
    method: 'get',
    url: '/v1_0/suggestion',
    params: {
      q: keyword
    }
  })
}
export function getSearch(keyword, page = 1) {
  return axios({
    method: 'get',
    url: '/v1_0/search',
    params: {
      q: keyword,
      page,
      per_page: 10
    }
  })
}
