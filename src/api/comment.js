import axios from '@/utils/request'
/**
 * 获取评论
 * @param {*} currentId
 * @param {*} offset
 * @returns
 */
export function getComment(articleId, offset) {
  return axios({
    method: 'GET',
    url: '/v1_0/comments',
    params: {
      type: 'a',
      source: articleId,
      offset: offset
    }
  })
}
/**
 * 添加评论
 * @param {*} articleId
 * @param {*} content
 * @returns
 */
export function addComment(articleId, content) {
  return axios({
    method: 'POST',
    url: '/v1_0/comments',
    data: {
      target: articleId,
      content
    }
  })
}
/**
 * 对评论点赞
 * @param {*} commentId
 * @returns
 */
export function addCommentZan(commentId) {
  return axios({
    url: '/v1_0/comment/likings',
    method: 'post',
    data: {
      target: commentId
    }
  })
}

/**
 * 取消点赞
 * @param {*} commentId
 * @returns
 */
export function delCommentZan(commentId) {
  return axios({
    url: '/v1_0/comment/likings/' + commentId,
    method: 'DELETE'
  })
}
/**
 * 获取回复列表
 * @param {*} commentId
 * @param {*} offset
 * @returns
 */
export function getReplyList(commentId, offset) {
  return axios({
    url: '/v1_0/comments',
    method: 'get',
    params: {
      type: 'c',
      source: commentId,
      offset
    }
  })
}
/**
 * 添加回复接口
 * @param {*} commentId
 * @param {*} content
 * @param {*} articleId
 * @returns
 */
export function addReply(commentId, content, articleId) {
  return axios({
    method: 'post',
    url: '/v1_0/comments',
    data: {
      target: commentId,
      content,
      art_id: articleId
    }
  })
}
